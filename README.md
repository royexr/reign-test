# Reign-test

Small web app for reign test

## Restore DB

to restore test db in mongo image, first unzip file in the path /db/dump.zip before create images

then, after run containers execute:

```
docker-compose exec -T db sh -c 'mongorestore dump'
```

## Development environment:

to run both services in development mode run:

### Create images

```
docker-compose -f docker-compose.yml build
```

### Run containers

```
docker-compose -f docker-compose.yml up -d
```

## Production environment

to run both services in production mode run:

### Create images

```
docker-compose -f docker-compose.yml -f docker-compose.prod.yml build
```

### Run containers

```
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d
```
