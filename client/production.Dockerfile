FROM node:14 as builder

COPY ["package.json", "package-lock.json", "/usr/src/"]

WORKDIR /usr/src

RUN npm install --only=production

COPY [".", "/usr/src/"]

RUN npm install --only=development

RUN npm run build

# Production image
FROM nginx

COPY --from=builder ["/usr/src/dist/client", "/usr/share/nginx/html"]

COPY ["/nginx-custom.conf", "/etc/nginx/conf.d/default.conf"]

EXPOSE 80
