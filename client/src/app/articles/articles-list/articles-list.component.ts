import { Component, OnDestroy, OnInit } from '@angular/core';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';
import { Article } from '../article.model';
import { ArticlesService } from '../articles.service';

@Component({
  selector: 'app-articles-list',
  templateUrl: './articles-list.component.html',
  styleUrls: ['./articles-list.component.sass'],
})
export class ArticlesListComponent implements OnInit, OnDestroy {
  faTrashAlt = faTrashAlt;
  articles: Article[] = [];
  subscription: Subscription = new Subscription();
  constructor(private articlesService: ArticlesService) {}

  ngOnInit(): void {
    this.subscription = this.articlesService.articlesChanged.subscribe(
      (articles: Article[]) => {
        this.articles = articles;
      }
    );
    this.articles = this.articlesService.getAll();
  }

  deleteArticle(id: string) {
    this.articlesService.delete(id);
  }

  redirect(article: Article) {
    if (article.story_url) {
      window.open(article.story_url, '_blank');
    } else if (article.url) {
      window.open(article.url, '_blank');
    } else {
      alert('Could not find url of the article!');
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
