import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticlesListComponent } from './articles-list/articles-list.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { CustomDatePipe } from './custom-date.pipe';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [ArticlesListComponent, CustomDatePipe],
  imports: [CommonModule, BrowserModule, HttpClientModule, FontAwesomeModule],
})
export class ArticlesModule {}
