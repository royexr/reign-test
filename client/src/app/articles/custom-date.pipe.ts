import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'customDate',
})
export class CustomDatePipe implements PipeTransform {
  transform(value: Date, ...args: unknown[]): unknown {
    const now = moment.utc();
    const created = moment.utc(value);

    if (now.get('date') - created.get('date') < 1) {
      return created.format('hh:mm a');
    } else if (now.get('date') - created.get('date') === 1) {
      return 'Yesterday';
    } else {
      return created.format('MMM Do');
    }
  }
}
