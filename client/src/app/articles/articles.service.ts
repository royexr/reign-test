import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Article } from './article.model';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root',
})
export class ArticlesService {
  articlesChanged = new Subject<Article[]>();
  private articles: Article[] = [];

  constructor(private httpClient: HttpClient) {}

  sortByDate(arts: Article[]) {
    return arts.sort((a, b) => {
      return (
        new Date(b.created_at).getTime() - new Date(a.created_at).getTime()
      );
    });
  }

  refresh() {
    this.httpClient
      .get<Article[]>(`${environment.apiUrl}/articles`, {
        responseType: 'json',
      })
      .subscribe((res) => {
        this.articles = this.sortByDate(res);
        this.articlesChanged.next(this.articles.slice());
      });

    return this.articles;
  }

  getAll() {
    this.refresh();
    return this.articles.slice();
  }

  delete(id: string) {
    this.httpClient
      .delete(`${environment.apiUrl}/articles/${id}`, {
        responseType: 'json',
      })
      .subscribe(() => {
        this.articlesChanged.next(this.refresh().slice());
      });
  }
}
