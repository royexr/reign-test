import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TasksModule } from './tasks/tasks.module';
import { ArticlesModule } from './articles/articles.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRoot(process.env.MONGO_URL, {
      useNewUrlParser: true,
      useFindAndModify: true,
      useCreateIndex: true,
    }),
    ScheduleModule.forRoot(),
    TasksModule,
    ArticlesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [MongooseModule, ScheduleModule],
})
export class AppModule {}
