import { HttpModule, Module } from '@nestjs/common';
import { ArticlesModule } from '../articles/articles.module';
import { TasksService } from './tasks.service';

@Module({
  imports: [HttpModule, ArticlesModule],
  providers: [TasksService],
  exports: [TasksService],
})
export class TasksModule {}
