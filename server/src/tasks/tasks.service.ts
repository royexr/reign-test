import { HttpService, Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { ArticlesService } from '../articles/articles.service';
import { Article } from '../articles/schemas/article.schema';

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);

  constructor(
    private articlesService: ArticlesService,
    private httpService: HttpService,
  ) {}

  // Execute call API every hour at second 0
  @Cron(CronExpression.EVERY_HOUR)
  getHits() {
    this.httpService
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .subscribe((res) => {
        const articles: Array<Article> = res.data.hits;
        articles.map((article) => {
          article.visible = true;
          this.articlesService.create(article).catch(() => {
            this.logger.debug(`Article ${article.objectID} already exists!`);
          });
        });
      });
  }
}
