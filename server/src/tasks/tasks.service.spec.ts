import { Test, TestingModule } from '@nestjs/testing';
import { ArticleModel } from '../articles/models/article.model';
import { TasksService } from './tasks.service';

describe('TasksService', () => {
  let tasksService: TasksService;
  const auxArticle = new ArticleModel(
    '5ffa1b8a35b8c7225c58f3a7',
    'jhoutromundo',
    new Date('2021-01-09T19:20:55.000+00:00'),
    '25703604',
    'Ask HN: Show me your half baked project',
    null,
    null,
    null,
    true,
  );

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TasksService,
        { provide: 'ArticlesService', useValue: {} },
        { provide: 'HttpService', useValue: {} },
      ],
    }).compile();

    tasksService = module.get<TasksService>(TasksService);
  });

  describe('Scheduled task serive operations', () => {
    it('should insert many Articles', async () => {
      const result: ArticleModel[] = [auxArticle];

      jest
        .spyOn(tasksService, 'getHits')
        .mockImplementation(() => Promise.resolve(result));

      expect(await tasksService.getHits()).toBe(result);
    });
  });
});
