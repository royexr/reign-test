import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';
import { ArticleModel } from './models/article.model';
import { Article } from './schemas/article.schema';

describe('ArticlesController', () => {
  let controller: ArticlesController;
  const auxArticle = new ArticleModel(
    '5ffa1b8a35b8c7225c58f3a7',
    'jhoutromundo',
    new Date('2021-01-09T19:20:55.000+00:00'),
    '25703604',
    'Ask HN: Show me your half baked project',
    null,
    null,
    null,
    true,
  );

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ArticlesController],
      providers: [
        ArticlesService,
        { provide: getModelToken(Article.name), useValue: { ...ArticleModel } },
      ],
    }).compile();

    controller = module.get<ArticlesController>(ArticlesController);
  });

  describe('CRUD controller operations', () => {
    it('should create an Article', async () => {
      const result: ArticleModel = auxArticle;

      jest
        .spyOn(controller, 'create')
        .mockImplementation(() => Promise.resolve(result));

      expect(await controller.create(result)).toBe(result);
    });

    it('should return all Articles', async () => {
      const result: ArticleModel[] = [auxArticle];

      jest
        .spyOn(controller, 'findAll')
        .mockImplementation(() => Promise.resolve(result));

      expect(await controller.findAll()).toBe(result);
    });

    it('should return an Article by id', async () => {
      const result: ArticleModel = auxArticle;

      jest
        .spyOn(controller, 'findOne')
        .mockImplementation(() => Promise.resolve(result));

      expect(await controller.findOne(result._id)).toBe(result);
    });

    it('should update an Article', async () => {
      const result: ArticleModel = auxArticle;

      jest
        .spyOn(controller, 'update')
        .mockImplementation(() => Promise.resolve(result));

      expect(await controller.update(result._id, result)).toBe(result);
    });

    it('should delete an Article', async () => {
      const id: string = auxArticle._id;
      const result: ArticleModel = auxArticle;

      jest.spyOn(controller, 'delete').mockImplementation(() => {
        result.visible = false;
        return Promise.resolve(result);
      });

      expect(await controller.delete(id)).toBe(result);
    });
  });
});
