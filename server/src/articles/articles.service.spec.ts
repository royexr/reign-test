import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { ArticlesService } from './articles.service';
import { ArticleModel } from './models/article.model';
import { Article } from './schemas/article.schema';

describe('ArticlesService', () => {
  let service: ArticlesService;
  const auxArticle = new ArticleModel(
    '5ffa1b8a35b8c7225c58f3a7',
    'jhoutromundo',
    new Date('2021-01-09T19:20:55.000+00:00'),
    '25703604',
    'Ask HN: Show me your half baked project',
    null,
    null,
    null,
    true,
  );

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticlesService,
        { provide: getModelToken(Article.name), useValue: { ...ArticleModel } },
      ],
    }).compile();

    service = module.get<ArticlesService>(ArticlesService);
  });

  describe('CRUD service operations', () => {
    it('should create an Article', async () => {
      const result: ArticleModel = auxArticle;

      jest
        .spyOn(service, 'create')
        .mockImplementation(() => Promise.resolve(result));

      expect(await service.create(result)).toBe(result);
    });

    it('should return all Articles', async () => {
      const result: ArticleModel[] = [auxArticle];

      jest
        .spyOn(service, 'findAll')
        .mockImplementation(() => Promise.resolve(result));

      expect(await service.findAll()).toBe(result);
    });

    it('should return an Article by id', async () => {
      const result: ArticleModel = auxArticle;

      jest
        .spyOn(service, 'findOne')
        .mockImplementation(() => Promise.resolve(result));

      expect(await service.findOne(result._id)).toBe(result);
    });

    it('should insert many Articles', async () => {
      const result: ArticleModel[] = [auxArticle];

      jest
        .spyOn(service, 'insertMany')
        .mockImplementation(() => Promise.resolve(result));

      expect(await service.insertMany(result)).toBe(result);
    });

    it('should update an Article', async () => {
      const result: ArticleModel = auxArticle;

      jest
        .spyOn(service, 'update')
        .mockImplementation(() => Promise.resolve(result));

      expect(await service.update(result._id, result)).toBe(result);
    });

    it('should delete an Article', async () => {
      const id: string = auxArticle._id;
      const result: ArticleModel = auxArticle;

      jest.spyOn(service, 'delete').mockImplementation(() => {
        result.visible = false;
        return Promise.resolve(result);
      });

      expect(await service.delete(id)).toBe(result);
    });
  });
});
