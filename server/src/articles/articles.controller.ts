import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  NotFoundException,
} from '@nestjs/common';
import { ArticlesService } from './articles.service';
import { CreateArticleDto } from './dto/create-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';

@Controller('articles')
export class ArticlesController {
  constructor(private readonly articlesService: ArticlesService) {}

  @Post()
  async create(@Body() createArticleDto: CreateArticleDto) {
    try {
      return this.articlesService.create(createArticleDto);
    } catch (error) {
      throw new NotFoundException();
    }
  }

  @Get()
  async findAll() {
    try {
      return this.articlesService.findAll();
    } catch (error) {
      throw new NotFoundException();
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      const res = await this.articlesService.findOne(id);
      if (!res) {
        throw new NotFoundException();
      } else {
        return res;
      }
    } catch (error) {
      throw new NotFoundException();
    }
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateArticleDto: UpdateArticleDto,
  ) {
    try {
      const res = await this.articlesService.update(id, updateArticleDto);
      if (!res) {
        throw new NotFoundException();
      } else {
        return res;
      }
    } catch (error) {
      throw new NotFoundException();
    }
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    try {
      const res = await this.articlesService.delete(id);
      if (!res) {
        throw new NotFoundException();
      } else {
        return res;
      }
    } catch (error) {
      throw new NotFoundException();
    }
  }
}
