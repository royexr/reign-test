import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
  @Prop()
  author: string;

  @Prop()
  created_at: Date;

  @Prop({ unique: true, required: true })
  objectID: string;

  @Prop()
  story_title: string;

  @Prop()
  story_url: string;

  @Prop()
  title: string;

  @Prop()
  url: string;

  @Prop()
  visible: boolean;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
