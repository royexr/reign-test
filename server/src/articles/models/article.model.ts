export class ArticleModel {
  constructor(
    public _id: string,
    public author: string,
    public created_at: Date,
    public objectID: string,
    public story_title: string,
    public story_url: string,
    public title: string,
    public url: string,
    public visible: boolean,
  ) {}
}
