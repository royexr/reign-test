import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateArticleDto } from './dto/create-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';
import { Article, ArticleDocument } from './schemas/article.schema';

@Injectable()
export class ArticlesService {
  constructor(
    @InjectModel(Article.name) private articleModel: Model<ArticleDocument>,
  ) {}

  async create(createArticleDto: CreateArticleDto): Promise<Article> {
    const article = new this.articleModel(createArticleDto);
    return article.save();
  }

  async findAll(): Promise<Article[]> {
    return this.articleModel.find();
  }

  async findOne(id: string): Promise<Article> {
    return this.articleModel.findById(id);
  }

  async insertMany(articles: Article[]): Promise<Article[]> {
    return this.articleModel.insertMany(articles);
  }

  async update(
    id: string,
    updateArticleDto: UpdateArticleDto,
  ): Promise<Article> {
    return this.articleModel.findByIdAndUpdate(id, updateArticleDto);
  }

  async delete(id: string): Promise<Article> {
    return this.articleModel.findByIdAndUpdate(id, { visible: false });
  }
}
