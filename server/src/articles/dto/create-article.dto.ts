import { IsString, IsDate } from 'class-validator';

export class CreateArticleDto {
  @IsString()
  author: string;

  @IsDate()
  created_at: Date;

  @IsString()
  objectID: string;

  @IsString()
  story_title: string;

  @IsString()
  story_url: string;

  @IsString()
  title: string;

  @IsString()
  url: string;
}
